# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

# lesson 13.4.1
require 'carrierwave/orm/activerecord'
